### Libraries and Plugin Used:
1. JADB (https://github.com/vidstige/jadb)
2. Hamcrest asserts (http://hamcrest.org/JavaHamcrest/)
3. Cucumber-jvm (https://cucumber.io/docs/reference/jvm#java)
4. Cucumber gradle parallel (https://github.com/camiloribeiro/cucumber-gradle-parallel)
5. flick (https://github.com/isonic1/flick)

### Pre-requisite:
1. Copy *.apk file to `/src/test/resources`
2. Provide below parameters to `AutomaitonConfig.properties`
    
    `app.path=<path/to/apk>`
    
    `carousell.username=<username>`
    
    `carousell.password=<password>`
    
    `video.capture=true/false`
    
    `video.format=mp4/gif`
     
### Components:
- Groovy scripts (Keep thin your automation code :) ):
    1. Appium server is separate task,and taken care by gradle based groovy scripts
    2. DeviceManager: Provides meta information of connected devices
    3. TaskExecutor: Driver script for cucumber
    4. Utils: Implementations of miscellaneous functions
- Step definitions: Contains implementations of cucumber steps, interacts with business flows 
- Business flows: Additional layers which can call other business flows or screen methods
- Screens: Implementation of pages for mobile apps
- Have descriptive logging using log4j
- Utilities:
    1. Custom Asserts: Extension of hemcrest assert, which provide crisp assert messages
    2. DriverFactory: Gives driver objects based on gradle task
    3. KEYS: An enum for relevant properties
    4. ScreenShotUtils: Captures screenshots of device/browsers, Also captures video recording and attached to reports

### Run:
`run=@Search ./gradlew clean runCukes`

### Reports:
1. After a run reports will be generated to /testRun/report_MMddyyhhmmss/cucumer/cucumber-html-reports/overview-features.html
2. Last screenshot of scenario also attached to report
3. Relevant screenshots and video recording capabilities (After each run it generates mp4 video
 recording of device, for iOS we can change it to gif)

### Reports Images:
![](images/Cucumber_Report_Overview.png)

![](images/Cucumber_Scenario.png)

### Video Recording

https://drive.google.com/open?id=1goaozrLE4BYptG--KPEFtfM0ToOjfryW



### Scope for further extensions:
1. Parallel or Distributed execution at feature and scenario levels across connected devices
2. Leverage for IOS
3. Integration with jenkins using jenkins build pipeline

### References:

1. Organize logic using build src 
https://docs.gradle.org/current/userguide/organizing_build_logic.html#sec:build_sources

2. Run cucumber parallel
https://github.com/camiloribeiro/cucumber-gradle-parallel

3. Cucumber and Infra
https://github.com/anandbagmar/cucumber-jvm-appium-infra
 