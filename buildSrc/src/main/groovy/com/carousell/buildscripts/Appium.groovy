package com.carousell.buildscripts

import io.appium.java_client.service.local.AppiumDriverLocalService
import io.appium.java_client.service.local.AppiumServiceBuilder
import io.appium.java_client.service.local.flags.GeneralServerFlag

import java.util.concurrent.TimeUnit

class Appium {
    static def startAppiumServer(String id, int appiumPort) {
        def reportsDir = Utils.project.ext.get("reportsDir")
        AppiumDriverLocalService service = null
        killExistingAppiumServerOnThisPortIfExisting(appiumPort)

        HashMap<String, String> osPaths = Utils.getOSSpecificPaths(System.getProperty("os.name").toLowerCase())
        println "[$id] Starting Appium Server on port - '$appiumPort' from - '${osPaths.toString()}'"
        if (osPaths.get("appiumPath") != null) {
            service = AppiumDriverLocalService.buildService(new AppiumServiceBuilder()
                    .usingDriverExecutable(new File(osPaths.get("nodePath")))
                    .withIPAddress("0.0.0.0")
                    .usingPort(appiumPort)
                    .withArgument(GeneralServerFlag.LOG_LEVEL, "warn:debug")
                    .withLogFile(new File(reportsDir + "/logs/appium.log"))
                    .withAppiumJS(new File(osPaths.get("appiumPath")))
                    .withStartUpTimeOut(2, TimeUnit.MINUTES))
            service.start()
            println "[$id] Appium Server started on port - '$appiumPort'"
        } else {
            throw new RuntimeException("[$id] ERROR - Appium path not provided")
        }
        service
    }

    static def killExistingAppiumServerOnThisPortIfExisting(int portNumber) {
        def seeCommand = 'ps aux | grep "[n]ode.*appium.*' + portNumber + '" | awk \'{print $2}\''
        def seeMessage = "See existing Appium Server on port - $portNumber, if already running using command - '$seeCommand'"
        Utils.runShellCommand(seeCommand, seeMessage)

        def killCommand = 'ps aux | grep "[n]ode.*appium.*' + portNumber + '" | awk \'{print $2}\' | xargs kill'
        def killMessage = "Kill existing Appium Server on port - $portNumber, if already running using command - '$killCommand'"
        Utils.runShellCommand(killCommand, killMessage)

        Utils.sleepFor(2)
    }
}