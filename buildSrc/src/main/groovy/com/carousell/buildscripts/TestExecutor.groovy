package com.carousell.buildscripts

class TestExecutor {
    static ArrayList<String> getListOfScenarios() {
        def stdoutScenarioNames = Utils.runShellCommand(getScenarioInformation(), "Get " +
                "list of applicable scenarios to " +
                "run", false)
        def sNames = stdoutScenarioNames.split("\n").findAll {
            (it.contains('Scenario:') || it.contains('Scenario Outline:')) && it.contains('.feature:')
        }.collect { (it.trim() =~ /Scenario(.?)*: (.*)#.*/)[0][2].trim() }
        println "Scenario Names - $sNames"
        sNames as ArrayList<String>
    }

    static def getScenarioInformation() {
        def cmd = "java -cp \"./build/libs/*\" cucumber.api.cli.Main -t 'not @pending' -t 'not @wip' -t 'not @todo' -t " +
                "'not @manual' " +
                "-g " +
                "com.carousell.automation.stepdefs src/test/resources/features/ -p pretty -d -m"
        println "Running command - $cmd"
        cmd
    }

    static
    def runTest(String reportsDir, String featureFileDir, String appiumPort, String deviceID, String platform, String deviceMake) {
        def envRunTags = System.getenv("run")
        def arglist = ["-p", "pretty", "-p", "json:" + reportsDir + "/cucumber/cucumber.json", "--glue", "com.carousell.automation",
                       featureFileDir, "-t", envRunTags]
        def log4jConfigPath = "src/test/resources/log4j2.properties"
        def runStatus = Utils.project.javaexec {
            ignoreExitValue = true
            main = "cucumber.api.cli.Main"
            classpath = Utils.project.configurations.cucumberRuntime + Utils.project.sourceSets.test.output
            environment "reportsDir", reportsDir
            args = arglist
            jvmArgs = ["-Dlog4j.configurationFile=" + log4jConfigPath,
                       "-DAppiumPort=" + appiumPort,
                       "-DDeviceID=" + deviceID,
                       "-DPlatform=" + platform,
                       "-DDeviceMake=" + deviceMake]
        }
        runStatus
    }
}
