package com.carousell.buildscripts

import se.vidstige.jadb.JadbConnection
import se.vidstige.jadb.JadbDevice

class DeviceManager {

    static ArrayList<JadbDevice> getMatchingAndroidDevices() {
        ArrayList<JadbDevice> matchingDevices = getDevices()
        println("No of attached devices: " + matchingDevices.size())
        if (matchingDevices.size() == 0)
            throw new RuntimeException("No device is attached")
        matchingDevices
    }

    private static def getDevices() {
        def hasEmulator = (new JadbConnection().devices.collect { device ->
            device.getSerial().contains("emulator")
        })
        if (hasEmulator != true) {
            new JadbConnection().devices.collect { device ->
                [
                        ID              : device.executeShell("getprop", "ro.serialno").text.trim(),
                        API_LEVEL       : device.executeShell("getprop", "ro.build.version.sdk").text.trim(),
                        DEVICE_MAKE     : device.executeShell("getprop", "ro.product.brand").text.trim(),
                        DEVICE_MODEL    : device.executeShell("getprop", "ro.product.model").text.trim(),
                        PLATFORM_VERSION: device.executeShell("getprop", "ro.build.version.release").text.trim(),
                        PLATFORM        : "Android"
                ]
            }
        } else {
            new JadbConnection().devices.collect { device ->
                [
                        ID              : device.serial.trim(),
                        API_LEVEL       : "NA",
                        DEVICE_MAKE     : "NA",
                        DEVICE_MODEL    : "NA",
                        PLATFORM_VERSION: "NA",
                        PLATFORM        : "Android"
                ]
            }
        }
    }
}