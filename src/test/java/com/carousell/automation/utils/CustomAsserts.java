package com.carousell.automation.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hamcrest.Matcher;

public class CustomAsserts {
    private static Logger LOGGER = LogManager.getLogger(CustomAsserts.class);

    public static <T> void assertThat(String reason, T actual, Matcher<? super T> matcher) {
        try {
            org.hamcrest.MatcherAssert.assertThat(reason, actual, matcher);
        } catch (AssertionError ae) {
            LOGGER.error("*****Custom Assertion Error : " + reason + "*****\n" + ae.toString());
            ScreenShotUtils.saveScreenShotAs(reason, reason);
            throw ae;
        }
    }

    public static void assertThat(String reason, boolean assertion) {
        try {
            org.hamcrest.MatcherAssert.assertThat(reason, assertion);
        } catch (AssertionError ae) {
            LOGGER.error("*****Custom Assertion Error : " + reason + "*****\n" + ae.toString());
            ScreenShotUtils.saveScreenShotAs(reason, reason);
            throw ae;
        }
    }
}
