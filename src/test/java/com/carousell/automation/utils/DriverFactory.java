package com.carousell.automation.utils;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.AutomationName;
import io.appium.java_client.remote.MobileCapabilityType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;
import java.net.URL;
import java.util.concurrent.TimeUnit;


public class DriverFactory {

    private static Logger LOGGER = LogManager.getLogger(DriverFactory.class);

    private ContextManager contextManager = ContextManager.getInstance();

    public AppiumDriver getDriver() throws Exception {

        String applicationType = contextManager.get(KEYS.APPLICATION_TYPE).toString();
        DesiredCapabilities capabilities;
        AppiumDriver appiumDriver = null;
        if (!applicationType.equalsIgnoreCase("Native")) {
            String browserType = contextManager.get(KEYS.BROWSER_TYPE).toString();
            LOGGER.debug("Initializing " + browserType);
            WebDriver webDriver = null;
            switch (browserType) {
                case BrowserType.FIREFOX:
                    System.setProperty("webdriver.gecko.driver", contextManager.get(KEYS.GECKO_DRIVER_PATH).toString());
                    capabilities = DesiredCapabilities.firefox();
                    capabilities.setCapability(FirefoxDriver.MARIONETTE, true);
                    FirefoxProfile geoDisabled = new FirefoxProfile();
                    geoDisabled.setPreference("geo.enabled", false);
                    geoDisabled.setPreference("geo.provider.use_corelocation", false);
                    geoDisabled.setPreference("geo.prompt.testing", false);
                    geoDisabled.setPreference("geo.prompt.testing.allow", false);
                    capabilities.setCapability(FirefoxDriver.PROFILE, geoDisabled);
                    webDriver = new FirefoxDriver();
                    break;
                case BrowserType.CHROME:
                    System.setProperty("webdriver.chrome.driver", contextManager.get(KEYS.CHROME_DRIVER_PATH).toString());
                    webDriver = new ChromeDriver();
                    break;
                default:
                    throw new IllegalArgumentException("Invalid browser type set in class injection " + browserType);
            }
            initWebDriver(webDriver);
        } else {
            String platformName = System.getProperty("Platform");
            String projectDir = System.getProperty("user.dir");
            String appPath = new File(projectDir + "/src/test/resources/" + contextManager.get(KEYS.APPLICATION_PATH))
                    .getAbsolutePath();
            if (platformName.equalsIgnoreCase("Android")) {
                capabilities = new DesiredCapabilities();
                capabilities.setCapability(MobileCapabilityType.APP, appPath);
                capabilities.setCapability(AndroidMobileCapabilityType.AUTO_GRANT_PERMISSIONS, true);
                capabilities.setCapability(AndroidMobileCapabilityType.APP_WAIT_ACTIVITY, contextManager.get(KEYS.APPLICATION_WAIT_ACTIVITY));
                capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, contextManager.get(KEYS.DEVICE_MAKE));
                capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, contextManager.get(KEYS.DEVICE_PLATFORM));
                capabilities.setCapability(MobileCapabilityType.UDID, contextManager.get(KEYS.DEVICE_ID));
                capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, AutomationName.ANDROID_UIAUTOMATOR2);
                capabilities.setCapability(MobileCapabilityType.FULL_RESET, true);
                URL serverUrl = new URL("http://0.0.0.0:" + contextManager.get(KEYS.APPIUM_PORT).toString() + "/wd/hub");
                appiumDriver = new AndroidDriver<>(serverUrl, capabilities);
            }
        }
        return appiumDriver;
    }

    private void initWebDriver(WebDriver webDriver) {
        webDriver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
        webDriver.manage().window().maximize();
    }

}
