package com.carousell.automation.businessflow;

import com.carousell.automation.screens.HomeScreen;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BrowseCategory extends BaseBusinessFlow{
    private static Logger LOGGER = LogManager.getLogger(BrowseCategory.class);

    public BrowseCategory() {
        super();
        LOGGER.debug("Instantiated " + BrowseCategory.class.getName());
    }

    public void selectFromHomeScreen(String category) {
        new HomeScreen().clickOnCategory(category);
    }
}
