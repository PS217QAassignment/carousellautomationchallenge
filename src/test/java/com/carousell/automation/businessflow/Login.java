package com.carousell.automation.businessflow;

import com.carousell.automation.screens.LoginOptionScreen;
import com.carousell.automation.utils.KEYS;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Login extends BaseBusinessFlow {
    private static Logger LOGGER = LogManager.getLogger(Login.class);

    public Login() {
        super();
        LOGGER.debug("Instantiated " + Login.class.getName());
    }

    public void existingUserLogin() {
        String userName = contextManager.get(KEYS.USERNAME).toString();
        String password = contextManager.get(KEYS.PASSWORD).toString();
        new LoginOptionScreen().loginWithCredentials(userName, password);
    }
}
