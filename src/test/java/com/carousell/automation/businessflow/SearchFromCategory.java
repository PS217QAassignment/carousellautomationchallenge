package com.carousell.automation.businessflow;

import com.carousell.automation.screens.ProductDetailScreen;
import com.carousell.automation.screens.SearchProductCategoryScreen;
import com.carousell.automation.stepdefs.BaseStepDefs;
import com.carousell.automation.utils.KEYS;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static com.carousell.automation.utils.CustomAsserts.assertThat;

public class SearchFromCategory extends BaseStepDefs {
    private static Logger LOGGER = LogManager.getLogger(SearchFromCategory.class);

    public SearchFromCategory() {
    }

    public void searchFor(String searchString) {
        String productCategory = (String) contextManager.get(KEYS.PRODUCTCATEGORY);
        LOGGER.info("Searching for " + searchString + " from " + productCategory);
        contextManager.addToContext(KEYS.SEARCHCRITERION, searchString);
        new SearchProductCategoryScreen().searchProductFrom(productCategory, searchString);
    }

    public void shouldHaveSearchResults() {
        LOGGER.info("Verify entered search has results");
        assertThat("Entered search has not find any result", new SearchProductCategoryScreen().hasSearchResults());
    }

    public void openFirstSearchResult() {
        LOGGER.info("Open first search result");
        new SearchProductCategoryScreen().scrollIfItHasPromotionalProducts().selectProductFromResults();
    }

    public void shouldHaveRelevantProductInformation() {
        LOGGER.info("Check that open product category screens has relevant details");
        assertThat("Product information does not have relevant info", new ProductDetailScreen().hasProductHaveRelevantInformation());
    }
}
