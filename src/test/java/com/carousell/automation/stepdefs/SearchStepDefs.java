package com.carousell.automation.stepdefs;

import com.carousell.automation.businessflow.SearchFromCategory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class SearchStepDefs extends BaseStepDefs {
    private static Logger LOGGER = LogManager.getLogger(SearchStepDefs.class);

    public SearchStepDefs() {
        LOGGER.debug("Instantiated " + SearchStepDefs.class.getName());

        When("I search for {string}", (String searchString) -> {
            String stepDef = "I search for " + searchString;
            LOGGER.info(stepDef);
            new SearchFromCategory().searchFor(searchString);
        });

        Then("I should get all results valid search results", () -> {
            String stepDef="I should get all results valid search results";
            LOGGER.info(stepDef);
            new SearchFromCategory().shouldHaveSearchResults();
        });

        When("I open first search result", () -> {
            String stepDef="I open first search result";
            LOGGER.info(stepDef);
            new SearchFromCategory().openFirstSearchResult();
        });

        Then("It should contained relevant information", () -> {
            String stepDef="It should contained relevant information";
            LOGGER.info(stepDef);
            new SearchFromCategory().shouldHaveRelevantProductInformation();
        });
    }
}
