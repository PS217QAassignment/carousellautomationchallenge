package com.carousell.automation.stepdefs;


import com.carousell.automation.utils.ContextManager;
import com.carousell.automation.utils.DriverFactory;
import com.carousell.automation.utils.KEYS;
import com.carousell.automation.utils.ScreenShotUtils;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;


public class Hooks {
    private ContextManager contextManager = ContextManager.getInstance();
    private static Logger LOGGER = LogManager.getLogger(Hooks.class);

    private void loadConfigPropertiesToContext() {
        try {
            LOGGER.debug("Loading automation properties");
            Properties properties = new Properties();
            String projectDir = System.getProperty("user.dir");
            FileInputStream inputStream = new FileInputStream(new File(projectDir + "/src/test/resources/AutomationConfig.properties"));
            properties.load(inputStream);
            contextManager.addToContext(KEYS.BROWSER_TYPE, System.getProperty("browser.type"));
            contextManager.addToContext(KEYS.GECKO_DRIVER_PATH, properties.getProperty("firefox.driver.path"));
            contextManager.addToContext(KEYS.CHROME_DRIVER_PATH, properties.getProperty("chrome.driver.path"));
            contextManager.addToContext(KEYS.APPLICATION_URL, properties.getProperty("application.url"));
            contextManager.addToContext(KEYS.APPLICATION_PATH, properties.getProperty("app.path"));
            contextManager.addToContext(KEYS.APPLICATION_WAIT_ACTIVITY, properties.getProperty("app.wait.activity"));
            contextManager.addToContext(KEYS.APPLICATION_ACTIVITY, properties.getProperty("app.activity"));
            contextManager.addToContext(KEYS.APPLICATION_PACKAGE, properties.getProperty("app.package"));
            contextManager.addToContext(KEYS.DEVICE_ID, System.getProperty("DeviceID"));
            contextManager.addToContext(KEYS.DEVICE_PLATFORM, System.getProperty("Platform"));
            contextManager.addToContext(KEYS.DEVICE_MAKE, System.getProperty("DeviceMake"));
            contextManager.addToContext(KEYS.APPIUM_PORT, System.getProperty("AppiumPort"));
            contextManager.addToContext(KEYS.APPLICATION_TYPE, properties.getProperty("app.type"));
            contextManager.addToContext(KEYS.USERNAME, properties.getProperty("carousell.username"));
            contextManager.addToContext(KEYS.PASSWORD, properties.getProperty("carousell.password"));
            contextManager.addToContext(KEYS.VIDEO_CAPTURE_ENABLE, properties.getProperty("video.capture"));
            contextManager.addToContext(KEYS.VIDEO_FORMAT, properties.getProperty("video.format"));
        } catch (IOException e) {
            LOGGER.debug("Properties file is  missing");
            e.printStackTrace();
        }
    }

    private void launchApplication() {
        DriverFactory driverFactory = new DriverFactory();
        try {
            contextManager.addToContext(KEYS.DRIVER, driverFactory.getDriver());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Before
    public void init(Scenario scenario) {
        loadConfigPropertiesToContext();
        String scenarioStartMessage = "\n" +
                "\n--------------------------------------------------------------------------------------------\n" +
                "\tScenario: '" + scenario.getName() + "\n" +
                "\n--------------------------------------------------------------------------------------------\n";
        LOGGER.debug(scenarioStartMessage);
        launchApplication();
        if (Boolean.parseBoolean(contextManager.get(KEYS.VIDEO_CAPTURE_ENABLE).toString()))
            ScreenShotUtils.startVideoRecording();
    }

    @After
    public void tearDown(Scenario scenario) {
        String scenarioEndMessage = "\n" +
                "\n--------------------------------------------------------------------------------------------\n" +
                "\tExecuted scenario  " + scenario.getName() + "\n " +
                "\tStatus : " + scenario.getStatus() + "\n" +
                "\n--------------------------------------------------------------------------------------------\n";
        LOGGER.debug(scenarioEndMessage);
        if (Boolean.parseBoolean(contextManager.get(KEYS.VIDEO_CAPTURE_ENABLE).toString()))
            ScreenShotUtils.embedVideoInReport(scenario);
        ScreenShotUtils.embedScreenShotInReport(scenario);
        ((WebDriver) contextManager.get(KEYS.DRIVER)).quit();
    }
}
