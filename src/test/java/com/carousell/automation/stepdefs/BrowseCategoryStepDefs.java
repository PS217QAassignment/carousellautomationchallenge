package com.carousell.automation.stepdefs;

import com.carousell.automation.businessflow.BrowseCategory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BrowseCategoryStepDefs extends BaseStepDefs {
    private static Logger LOGGER = LogManager.getLogger(BrowseCategoryStepDefs.class);

    public BrowseCategoryStepDefs(){
        LOGGER.debug("Instantiated " + BrowseCategoryStepDefs.class.getName());
        When("I select {string} from available product categories", (String category) -> {
            String stepDef = "I select " + category + " from available product categories";
            LOGGER.info(stepDef);
            new BrowseCategory().selectFromHomeScreen(category);
        });
    }
}
