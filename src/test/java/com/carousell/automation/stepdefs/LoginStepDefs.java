package com.carousell.automation.stepdefs;

import com.carousell.automation.businessflow.Login;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class LoginStepDefs extends BaseStepDefs {
    private static Logger LOGGER = LogManager.getLogger(LoginStepDefs.class);

    public LoginStepDefs() {
        LOGGER.debug("Instantiated " + LoginStepDefs.class.getName());

        Given("^I login with my credentials$", () -> {
            String stepDef = "I login with my credentials";
            LOGGER.info(stepDef);
            new Login().existingUserLogin();
        });
    }

}
