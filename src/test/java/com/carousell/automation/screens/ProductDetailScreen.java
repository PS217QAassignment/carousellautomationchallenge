package com.carousell.automation.screens;

import com.carousell.automation.utils.KEYS;
import io.appium.java_client.MobileBy;

public class ProductDetailScreen extends AbstractScreen{
    private String productTitleLabel = "new UiSelector().resourceId(\"com.thecarousell.Carousell:id/tvInfo\")";

    public boolean hasProductHaveRelevantInformation() {
        LOGGER.debug("Checking for productcategoryscreens label");
        waitForElementToBeClickable(MobileBy.AndroidUIAutomator(productTitleLabel));
        String actualProductLabelText = driver.findElement(MobileBy.AndroidUIAutomator(productTitleLabel)).getText().toLowerCase();
        String expectedProductLabelText = contextManager.get(KEYS.SEARCHCRITERION).toString().toLowerCase();
        return actualProductLabelText.contains(expectedProductLabelText);
    }


}
