package com.carousell.automation.screens.productcategoryscreens;

import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.WebElement;

import java.util.List;

public class CarsCategoryScreen extends BaseProductScreen {
    private String forSaleCategory = "new UiSelector().textContains(\"For Sale\")";

    private String searchCarEditTextLocator = "new UiSelector().resourceId(\"com.thecarousell.Carousell:id/tv_search\")";

    private String searchEditBoxOnRVLocator = "new UiSelector().resourceId(\"com.thecarousell.Carousell:id/et_search\")";

    private String listOfSearchResultLocator = "new UiSelector().resourceId(\"com.thecarousell.Carousell:id/tv_title\")";

    private String searchButtonLocator = "new UiSelector().resourceId(\"com.thecarousell.Carousell:id/btn_search\")";

    @Override
    public void searchProduct(String searchString) {
        clickOnElement(MobileBy.AndroidUIAutomator(forSaleCategory));
        clickOnElement(MobileBy.AndroidUIAutomator(searchCarEditTextLocator));
        clickOnElement(MobileBy.AndroidUIAutomator(searchEditBoxOnRVLocator));
        enterTextTo(MobileBy.AndroidUIAutomator(searchEditBoxOnRVLocator), searchString);
        List<WebElement> listOfSearchResults =
                driver.findElements(MobileBy.AndroidUIAutomator(listOfSearchResultLocator));
        clickOnElement((MobileElement) listOfSearchResults.stream().filter(webElement -> webElement.getText().equalsIgnoreCase(searchString)).findFirst().get());
        clickOnElement(MobileBy.AndroidUIAutomator(searchButtonLocator));
        dismissFeatureDialog();
    }
}
