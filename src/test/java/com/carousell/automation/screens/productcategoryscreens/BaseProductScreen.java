package com.carousell.automation.screens.productcategoryscreens;

import com.carousell.automation.screens.AbstractScreen;
import com.carousell.automation.utils.ContextManager;
import org.openqa.selenium.WebDriver;

public abstract class BaseProductScreen extends AbstractScreen {
    protected ContextManager contextManager = ContextManager.getInstance();
    protected WebDriver driver = contextManager.driver();

    public void searchProduct(String searchString) {

    }
}
