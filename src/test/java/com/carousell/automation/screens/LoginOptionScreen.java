package com.carousell.automation.screens;

import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class LoginOptionScreen extends AbstractScreen {
    private static Logger LOGGER = LogManager.getLogger(LoginOptionScreen.class);

    private String loginButtonLocator = "new UiSelector().resourceId(\"com.thecarousell.Carousell:id/welcome_page_login_button\")";
    private String credentialPickerLocator = "new UiSelector().resourceId(\"com.google.android.gms:id/credential_picker_layout\")";
    private String listOfEditBoxLocator = "new UiSelector().className(\"android.widget.EditText\")";
    private String loginSubmitButtonLocator = "new UiSelector().resourceId(\"com.thecarousell.Carousell:id/login_page_login_button\")";
    private String noneOfAboveButtonLocator = "new UiSelector().resourceId(\"com.google.android.gms:id/cancel\")";

    public LoginOptionScreen() {
        super();
        LOGGER.debug("Instantiated " + LoginOptionScreen.class.getName());
    }

    public void loginWithCredentials(String userName, String password) {
        clickOnElement(MobileBy.AndroidUIAutomator(loginButtonLocator));

        if(waitForElementToBeClickable(MobileBy.AndroidUIAutomator(credentialPickerLocator),5))
            clickOnElement(MobileBy.AndroidUIAutomator(noneOfAboveButtonLocator));
        enterTextTo((MobileElement) driver.findElements(MobileBy.AndroidUIAutomator(listOfEditBoxLocator)).get(0), userName);
        enterTextTo((MobileElement) driver.findElements(MobileBy.AndroidUIAutomator(listOfEditBoxLocator)).get(1), password);
        clickOnElement(MobileBy.AndroidUIAutomator(loginSubmitButtonLocator));
    }

}
