package com.carousell.automation.screens;

import com.carousell.automation.screens.productcategoryscreens.BaseProductScreen;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.Dimension;

import java.util.List;
import java.util.Objects;

public class SearchProductCategoryScreen extends AbstractScreen {

    private String resultItemsLocator = "new UiSelector().resourceId(\"com.thecarousell.Carousell:id/card_product\")";

    public void searchProductFrom(String productCategory, String searchString) {
        BaseProductScreen productScreen = ProductCategoryScreenFactory.getProductScreen(productCategory);
        Objects.requireNonNull(productScreen).searchProduct(searchString);
    }

    public boolean hasSearchResults() {
        return waitForElementToBeClickable(MobileBy.AndroidUIAutomator(resultItemsLocator));
    }

    public SearchProductCategoryScreen scrollIfItHasPromotionalProducts() {
        String promotionalLabelLocator = "new UiSelector().textContains(\"Promoted\")";
        String spotlightLabelLocator = "new UiSelector().textContains(\"Spotlight\")";
        if (waitForElementToBeClickable(MobileBy.AndroidUIAutomator(promotionalLabelLocator), 5)
                || waitForElementToBeClickable(MobileBy.AndroidUIAutomator(spotlightLabelLocator), 5)) {
            Dimension size = driver.manage().window().getSize();
            int startY = (int) (size.height * 0.70);
            int endY = (int) (size.height * 0.30);
            int endX = (size.width / 2);
            int startX = 0;
            scrollTo(startY, endY, endX, startX);
        }
        return this;
    }


    public void selectProductFromResults() {
        List listOfResultsLocator = driver.findElements(MobileBy.AndroidUIAutomator(resultItemsLocator));
        clickOnElement((MobileElement) listOfResultsLocator.get(2));
        dismissFeatureDialog();
        dismissFeatureDialog();
    }
}

