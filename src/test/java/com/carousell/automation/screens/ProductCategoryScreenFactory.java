package com.carousell.automation.screens;

import com.carousell.automation.screens.productcategoryscreens.BaseProductScreen;
import com.carousell.automation.screens.productcategoryscreens.CarsCategoryScreen;
import com.carousell.automation.screens.productcategoryscreens.FlightCategoryScreen;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ProductCategoryScreenFactory {
    protected static Logger LOGGER = LogManager.getLogger(AbstractScreen.class);

    static BaseProductScreen getProductScreen(String productCategory) {
        LOGGER.debug("Creating screen object for " + productCategory);
        if (productCategory.equalsIgnoreCase("Cars"))
            return new CarsCategoryScreen();
        else if (productCategory.equalsIgnoreCase("Flight"))
            return new FlightCategoryScreen();
        else
            return null;
    }
}
