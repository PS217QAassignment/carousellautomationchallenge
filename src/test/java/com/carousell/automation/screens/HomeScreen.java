package com.carousell.automation.screens;

import com.carousell.automation.utils.KEYS;
import io.appium.java_client.MobileBy;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class HomeScreen extends AbstractScreen {
    private static Logger LOGGER = LogManager.getLogger(HomeScreen.class);

    public HomeScreen() {
        super();
        LOGGER.debug("Instantiated " + HomeScreen.class.getName());
    }

    public void clickOnCategory(String category) {
        String categoryLocator = "new UiSelector().textContains(\"" + category + "\")";
        contextManager.addToContext(KEYS.PRODUCTCATEGORY,category);
        clickOnElement(MobileBy.AndroidUIAutomator(categoryLocator));
    }
}
