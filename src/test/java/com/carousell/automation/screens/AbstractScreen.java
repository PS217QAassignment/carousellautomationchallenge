package com.carousell.automation.screens;

import com.carousell.automation.utils.ContextManager;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.InvalidElementStateException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public abstract class AbstractScreen {

    protected static Logger LOGGER = LogManager.getLogger(AbstractScreen.class);
    protected ContextManager contextManager = ContextManager.getInstance();
    AppiumDriver driver;

    protected AbstractScreen() {
        driver = contextManager.driver();
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    private boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    private void clickOnElement(By by, int timeout) {
        waitForElementToBeClickable(by, timeout);
        clickOnElement((MobileElement) driver.findElement(by));
    }

    boolean waitForElementToBeClickable(By by) {
        return waitForElementToBeClickable(by, 30);
    }

    boolean waitForElementToBeClickable(By by, int timeout) {
        try {
            WebDriverWait wait = new WebDriverWait(driver, timeout);
            return wait.until((ExpectedCondition<Boolean>) driver -> isElementPresent(by));
        }catch (TimeoutException ex){
            LOGGER.debug("Element not found " + by.toString());
            return false;
        }
    }

    protected void clickOnElement(By by) {
        clickOnElement(by, 30);
    }

    protected void clickOnElement(MobileElement mobileElement) {
        clickOnElement(mobileElement, 30);
    }

    private void clickOnElement(MobileElement mobileElement, int timeout) {
        LOGGER.debug("clicking on " + mobileElement.toString());
        if (waitForElementToBeClickable(mobileElement, timeout)) {
            mobileElement.click();
        }
    }

    protected void enterTextTo(By by, String textValue) {
        enterTextTo(by, textValue, 30);
    }

    private void enterTextTo(By by, String textValue, int timeout) {
        LOGGER.debug("Enter " + textValue + " in  " + by.toString());
        MobileElement mobileElement;
        if (waitForElementToBeClickable(by)) {
            mobileElement = (MobileElement) driver.findElement(by);
            enterTextTo(mobileElement, textValue);
        }
    }

    void enterTextTo(MobileElement mobileElement, String textValue) {
        enterTextTo(mobileElement, textValue, 30);
    }

    private void enterTextTo(MobileElement mobileElement, String textValue, int timeout) {
        LOGGER.debug("Enter " + textValue + " in  " + mobileElement.toString());
        if (waitForElementToBeClickable(mobileElement, timeout)) {
            try {
                mobileElement.clear();
            } catch (InvalidElementStateException ex) {
                LOGGER.debug(ex.getMessage());
            }
            mobileElement.sendKeys(textValue);
        }
    }

    private boolean waitForElementToBeClickable(MobileElement mobileElement, int timeout) {
        try {
            WebDriverWait wait = new WebDriverWait(driver, timeout);
            wait.until(ExpectedConditions.elementToBeClickable(mobileElement));
            return true;
        } catch (TimeoutException | NoSuchElementException ex) {
            LOGGER.debug("Element not visible " + mobileElement.toString());
            return false;
        }
    }

    void scrollTo(int startY, int endY, int endX, int startX) {
        TouchAction action = new TouchAction(driver);
        action
                .press(PointOption.point(startX, startY))
                .waitAction(WaitOptions.waitOptions(Duration.ofMillis(700)))
                .moveTo(PointOption.point(endX,endY))
                .release()
                .perform();
    }

    protected void dismissFeatureDialog() {
        String notificationLocator = "new UiSelector().resourceId(\"com.thecarousell.Carousell:id/feature_dialog\")";
        String gotItButtonLocator = "new UiSelector().resourceId(\"com.thecarousell.Carousell:id/feature_button\")";
        if(waitForElementToBeClickable(MobileBy.AndroidUIAutomator(notificationLocator)))
            clickOnElement(MobileBy.AndroidUIAutomator(gotItButtonLocator));
    }

}
