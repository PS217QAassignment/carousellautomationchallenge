Feature: Carousell search across different categories
  As a application user, I want to be able to search items across different categories

  @Search @Android
  Scenario: Search different cars
    Given I login with my credentials

    When I select "cars" from available product categories
    And I search for "Porsche"
    Then I should get all results valid search results
    When I open first search result
    Then It should contained relevant information